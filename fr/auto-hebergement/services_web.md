Services web
============

Cette partie va présenter l’installation de quelques services web, comme
un blog, un webmail ou un forum.

On supposera que nginx et php sont déjà installés comme expliqué à la partie
[nginx](installation.html#installation-de-nginx), et que vous avez généré des certificats SSL comme indiqué plus
loin (voir la section [ssl](#obtenir-un-certificat-ssl)).

Afin de décrire la difficulté de l’installation, on va mettre des
petites étoiles ★ :

-   ★ : installation facile. Pas de base de données, très peu de
    manipulations à faire.

-   ★★ : difficulté intermédiaire.

-   ★★★ : installation avancée. Une base de données
    sera certainement à mettre en place.

Obtenir un certificat SSL
-------------------------

Afin que les données ne circulent pas en clair sur votre site, il faut
utiliser le chiffrement ssl. C’est important lorsque des mots de passes
sont utilisés pour se connecter (webmail, forum…).

<p class="alert alert-warning">
Pour les plus intéressés, vous pourrez tester la sécurité de votre serveur sur le site suivant :
<a href="https://www.ssllabs.com/">https://www.ssllabs.com/</a>, qui fournit
d’excellents conseils pour l’améliorer.
</p>

### Obtenir un certificat avec letsencrypt

Le site <https://letsencrypt.org/> fournit un client permettant
d’obtenir un certificat qui sera automatiquement considéré “de
confiance” par tous les navigateurs. C’est un service tout à fait adapté
à l’auto-hébergement qui est tout bonnement génial.

Voici comment l’utiliser avec nginx, vous allez voir, c’est vraiment
très simple.

Tout d’abord, installez letsencrypt. Sur debian, vous pouvez l’installer
à partir des dépots backports à l’heure où j’écris ces lignes :

```
# apt-get -t jessie-backport install letsencrypt
```

Sinon, récupérez-le avec la commande git (installez le paquet git
auparavant si besoin)

```
$ git clone https://github.com/letsencrypt/letsencrypt
$ cd letsencrypt
```

Dans le ce second cas, la suite sera à réaliser avec la commande
`./letsencrypt-auto` dans le dossier de letsencrypt à la place de la
commande simple `letsencrypt`.

Voici comment obtenir un certificat :

```
letsencrypt certonly --webroot --rsa-key-size 4096 \
    -w /chemin/vers/votre/serveur -d votredomaine.com -d www.votredomaine.com
```

Remplacez les éléments suivants :

-   `/chemin/vers/votre/serveur` : il faut indiquer le dossier qui
    contient votre site web
-   `-d votredomaine.com` : remplacez par votre nom de domaine. Notez
    que vous pouvez ajouter autant de domaines et sous-domaines que vous
    voulez en rajoutant une option `-d` à chaque fois.

À l’avenir, pour renouveler les certificats, il faudra simplement lancer
`letsencrypt renew`. En cas de doute, n’hésitez pas à vous référer à la
documentation officielle[^1].

Le certificat obtenu se trouve dans le dossier
`/etc/letsencrypt/live/votredomaine.com/`. Voici donc un extrait de la
configuration de nginx à indiquer pour préciser le certificat :

```
server {
    listen 443 ssl http2;
    server_name votredomaine.com;
    ssl_certificate /etc/letsencrypt/live/votredomaine.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/votredomaine.com/privkey.pem;
    [. . .]
}
```

Tout ceci doit se faire pendant que nginx est lancé. Une fois les
modifications apportées, rechargez-le avec la commande

```
# service nginx reload
```

### Générer un certificat SSL auto-signé

Nous allons ici auto-signer le certificat. Les visiteurs de votre site
risquent juste d’avoir un avertissement de ce type :

![](images/avertissement-ssl.png)

Sachez qu’il est possible d’acheter une autorité de certification. Mais
dépenser votre argent n’est pas forcément nécessaire, et un certificat
auto-signé ne retire en rien la protection du chiffrement ssl.

Tout d’abord, il faut installer quelques paquets :

```
# apt-get install openssl ssl-cert
```

Pour créer un certificat et le signer, il faut ensuite lancer la
commande suivante. Bien sûr, remplacez le nom du fichier certificat.pem
à votre convenance :

```
# openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 \
    -keyout /etc/ssl/private/certificat.pem \
    -out /etc/ssl/private/certificat.pem
```

Quelques questions vous seront posées. Vous n’êtes pas obligé de remplir
tous les champs.

Finalement, il faut protéger ce certificat. Il faudra alors lancer ces
deux dernières commandes afin d’en restreindre les droits d’accès :

```
# chown root:root /etc/ssl/private/certificat.pem
# chmod 600 /etc/ssl/private/certificat.pem
```

Retenez bien le chemin vers ce certificat. Il faudra le préciser dans la
configuration de nginx par la suite. Vous devrez alors modifier dans les
exemples donnés les variables suivantes :

```
ssl_certificate /etc/ssl/private/mondomaine.pem;
ssl_certificate_key /etc/ssl/private/mondomaine.pem;"
```

Exemple simple : NoNonsense Forum ★
------------------------------------------

On va commencer par décrire l’installation d’un petit bijou :
[NoNonsenseForum](http://camendesign.com/nononsense_forum), un forum
minimaliste, sans base de données, et pourtant génial.

Avec ce forum, pas de compte à créer avec confirmation par mail. Vous
choisissez un pseudo lorsque vous envoyer un message, puis vous le
réservez avec un mot de passe. Ainsi pas d’inscription, juste un mot de
passe à retenir.

Pour l’utiliser et l’installer, vous n’aurez besoin que d’installer le
paquet php5 et d’avoir un serveur http fonctionnel. Suivez les
indications aux section [nginx et php](installation.html#installation-de-nginx).

![](images/nononsense.png)

### Installation du forum

Passons à l’installation du forum, qui est une méthode assez classique
comme vous le constaterez pour d’autres services:

-   Tout d’abord, on télécharge l’archive à cette adresse
    <https://github.com/Kroc/NoNonsenseForum/archive/master.zip>. On
    utilise pour cela l’outil `wget`.

    ```
    $ wget https://github.com/Kroc/NoNonsenseForum/archive/master.zip
    ```

-   On la décompresse avec unzip :

    ```
    $ unzip master.zip
    ```

    Un dossier NoNonsenseForum-master est créé.

-   On s’assure que ce dossier appartient au bon utilisateur :

    ```
    # chown -R www-data:www-data NoNonsenseForum-master
    ```

-   Dans le dossier NoNonsenseForum-master, on copie le fichier de
    configuration car on n’utilisera pas le fichier .htaccess avec nginx
    :

    ```
    $ cp config.default.php config.php
    ```

    Dans ce fichier, on précise le dossier contenant les informations
    des utilisateurs :

    ```
    @define ('FORUM_USERS',         '../forum_infos');

    ```

-   On crée le dossier `forum_infos` puis on change le propriétaire :

    ```
    # mkdir forum_infos
    # chown www-data:www-data forum_infos

    ```

### Configuration de nginx pour le forum

Voici ce qu’il faudra mettre dans un nouveau fichier de configuration
pour nginx, par exemple dans `/etc/nginx/conf.d/forum.conf`

```
# Forum
server {
listen 80;
server_name forum.mondomaine.net;
index index.php;
root /chemin/vers/NoNonsenseForum-master;
        location ~ \.php$ {
                try_files $uri = 404;
                fastcgi_pass unix:/var/run/php5-fpm.sock;
                include fastcgi_params;
        }
}
```

N’oubliez pas de modifier les variables `server_name` et `root`, puis
relancez nginx avec :

```
# service nginx restart
```

Et voilà, votre forum est accessible sur http://forum.mondomaine.net.

Un blog
-------

Il existe une multitude de moteurs de blog, plus ou moins complets, et
donc plus ou moins faciles à installer.

### Blogotext ★

[Blogotext](http://lehollandaisvolant.net/blogotext/fr/) est un moteur
de blog très léger et pourtant très puissant. Il s’avère être très bien
pensé et constitue à lui seul un outil complet pour qui veut publier sur
le web. En effet, sa description indique ses nombreuses fonctionnalités.
En moins de 1 Mo, il vous permet de :

-   publier un blog
-   partager/sauvegarder des liens
-   partager des fichiers et des images
-   faire un micro-blogging
-   lire vos flux RSS

Pour la suite, nous supposerons que vous voulez avoir votre blog sur le
sous-domaine `blog.mondomaine.com`.

<div class="alert alert-warning">
    <p>Petite astuce pour renforcer la sécurité de votre blog après
l’installation de blogotext. Renommez le dossier <code>admin</code>
situé dans le dossier <code>blogotext</code> avec ces deux commandes.
    <pre>cd /var/messites/blogotext</pre>
    mv admin nouveau-nom</pre>
    <p>Retenez-bien son nouveau nom.</p>
    <p>C’est maintenant à l’adresse
<code>https://blog.mondomaine.com/nouveau-nom</code> que vous pourrez
administrer votre blog.</p>
</div>

#### Installation de Blogotext :

Pour installer blogotext, vous aurez besoin d’installer nginx et php. Il
s’agit en fait des paquets ci-dessous (Notez que certains sont
normalement déjà installés suite aux paragraphes [nginx et php](installation.html#installation-de-nginx).

```
# apt-get install nginx openssl ssl-cert php5-fpm
php5-fpm php-apcu php5-gd unzip sqlite php5-sqlite
```

On peut maintenant télécharger blogotext :

```
wget "http://lehollandaisvolant.net/blogotext/blogotext.zip"
```

Ensuite, on décompresse l’archive :

```
unzip blogotext.zip
```

Vous voilà avec un dossier `blogotext`. Je vous propose de le déplacer
dans le dossier `/var/messites` :

```
mkdir -p /var/messites
mv blogotext /var/messites/
```

Avant d’aller plus loin, modifions les droits du dossier de blogotext :

```
chown -R www-data:www-data /var/messites/blogotext
```
<p class="alert alert-warning">
La commande ci-dessus restreint le site à l’utilisateur
www-data. C’est un utilisateur un peu spécial, qui est en fait
nginx. Cela renforce la sécurité de votre serveur
</p>

Nous pouvons désormais passer à la configuration de nginx. Créez un
nouveau fichier de configuration, par exemple dans
`/etc/nginx/conf.d/blog.conf` :

```
server {
    listen 80;
    server_name blog.mondomaine.com;
    return 301 https://$server_name$request_uri;  # enforce https
}
server {
    listen 443 ssl http2;
    ssl_certificate /etc/ssl/private/mondomaine.pem;
    ssl_certificate_key /etc/ssl/private/mondomaine.pem;"
    server_name blog.mondomaine.com;
    index index.php;
    root /var/messites/blogotext;
    client_max_body_size 1500M;

    location ~ \.php$ {
        try_files $uri = 404;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        include fastcgi_params;
        fastcgi_intercept_errors on;
        fastcgi_param HTTPS on;
        fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
```

Comme toujours, quelques modifications seront nécessaires. Adaptez à
votre cas les variables `server_name`, `ssl_certificate`,
`ssl_certificate_key` (voir [ssl](#obtenir-un-certificat-ssl)) et `root`.

Une fois ceci fait, vous pourrez aller terminer l’installation de
blogotext à l’adresse https://blog.mondomaine.com après avoir rechargé
nginx :

```
service nginx restart
```

### PluXML ★

PluXML est un autre moteur de blog. On va utiliser ici le paquet debian
pour l’installer. Notez qu’il est tout à fait possible d’installer
PluXML à partir d’une archive téléchargée sur le site officiel, mais
cela veut dire que vous savez installer les dépendances tout seul.

```
# apt-get install pluxml
```

On vous demande à l’installation le mot de passe de l’administrateur du
site. Tout le reste est géré par le paquet.

![](images/pluxmlinstall.png)

Ensuite, il n’y a plus qu’à configurer nginx avec le fichier suivant à
enregistrer dans `/etc/nginx/conf.d/pluxml.conf`

```
server {
  listen      443 ssl;
  ssl_certificate /etc/ssl/private/votreserveur.pem;
  ssl_certificate_key /etc/ssl/private/votreserveur.pem;
  server_name blog.monsite.com;
  root /usr/share/pluxml;
  index index.php;
  location ~ \.php$ {
    try_files $uri = 404;
    fastcgi_pass unix:/var/run/php5-fpm.sock;
    include fastcgi_params;
    fastcgi_intercept_errors on;
    fastcgi_param HTTPS on;
    fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
  }
# On cache le fichier version:
    location /version {
    return 404;
  }
# Ligne tres importante pour eviter le vol de mot de passe
    location /data/configuration/users.xml {
    return 403;
  }
# On cache le dossier update
    location /update {
    return 404;
  }
# URL Rewriting
    if (!-e $request_filename) {
    rewrite ^/([^feed\/].*)$ /index.php?$1 last;
  }
  rewrite ^/feed\/(.*)$ /feed.php?$1 last;
    # Interdir l'acces au repertoire contenant un fichier .htaccess
    location ~ /\.ht {
    deny  all;
  }
}
```

Modifiez les variables `server_name` et celles commençant par `ssl_`
pour les adapter à votre cas.

Rechargez nginx :

```
# service nginx restart
```

Et voilà, pluxml est maintenant accessible  :). Pour votre
première connexion, le login est “admin” avec le mot de passe définit
au-dessus.

Webmail
-------

Un webmail est une interface que l’on ouvre dans un navigateur web, qui
permet de consulter sa messagerie. Il en existe plusieurs, c’est
pourquoi les exemples présentés ensuite ne sont pas exhaustifs.

### Squirrelmail ★

[Squirrelmail](https://squirrelmail.org/) est un webmail dont le look
peut paraître démodé. Cependant, il fonctionne très bien et sait se
montrer léger tout en restant facile à utiliser.

![](images/squirrelmail-list.jpg)

![](images/squirrelmail-read.jpg)

Sur debian, squirrelmail et ses dépendances s’installent ainsi :

```
# apt-get install squirrelmail squirrelmail-locales \
    php5-fpm php5 php5-common
```

Vous pouvez compléter l’installation de squirrelmail avec les plugins.
Des paquets sont disponibles sur debian. Pour les voir, utilisez la
commande `apt-cache search squirrelmail`.

Maintenant, il faut configurer squirrelmail. Pour nous, pas grand chose
à faire.

-   Lancez `# squirrelmail-configure`
-   Tapez `D`, puis entrée.
-   Tapez `dovecot` puis entrée.
-   Tapez `S` puis entrée.
-   (facultatif) Tapez `10` puis entrée : `Default language : fr_FR` et
    `Default Charset : UTF-8`.
-   Le menu `8` permet d’activer d’éventuels plugins.
-   Toujours terminer par `S` pour sauvegarder, puis pour quitter il
    faut taper `Q`.

Il reste à ajouter un site à nginx. Copiez la configuration ci-dessous
dans `/etc/nginx/conf.d`.

N’oubliez pas de modifier :

-   `server_name` en y mettant l’adresse de votre webmail à taper dans
    un navigateur. Cette adresse sera en
    https://webmail.votredomaine.net.
-   Le chemin vers votre certificat ssl (voir la section [ssl](#obtenir-un-certificat-ssl)) aux variables
    `ssl_certificate` et `ssl_certificate_key`

```
# Squirrelmail
server {
    listen 443 ssl http2;
    server_name webmail.votredomaine.net;
    index index.php index.html;
    root /usr/share/squirrelmail;

    ssl_certificate /etc/ssl/private/votreserveur.pem;
    ssl_certificate_key /etc/ssl/private/votreserveur.pem;

    location / {

    ## All regex locations are nested for easier maintenance.

    ## Static files are served directly.
    location ~* .(?:css|gif|jpe?g|js|png|swf)$ {
        expires max;
        log_not_found off;
        ## No need to bleed constant updates. Send the all shebang in one
        ## fell swoop.
        tcp_nodelay off;
        ## Set the OS file cache.
        open_file_cache max=500 inactive=120s;
        open_file_cache_valid 45s;
        open_file_cache_min_uses 2;
        open_file_cache_errors off;
    }

    ## Keep a tab on the 'big' static files.
    location ~* ^.+.(?:m4a|mp[34]|mov|ogg|flv|pdf|ppt[x]*)$ {
        expires 30d;
        ## No need to bleed constant updates. Send the all shebang in one
        ## fell swoop.
        tcp_nodelay off;
    }

    ## All PHP files that are to be directly processed by the FCGI
    ## upstream are on the src subdirectory of the squirrelmail
    ## distribution.
    location ^~ /src/ {
        location ~* ^/src/[[:alnum:]_]+.php$ {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
        }
    }

    ## Plugins directory. This is used in the case you're using plugins.
    location ^~ /plugins/ {
        location ~* ^/plugins/[[:alnum:]]+/[[:alnum:]_]+.php$ {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
        }
    }

    ## The default index handler.
    location = /index.php {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    ## Protect the web root configure.
    location = /configure {
        return 404;
    }

    ## If no favicon exists return a 204 (no content error).
    location = /favicon.ico {
        try_files $uri @empty;
        log_not_found off;
        access_log off;
    }

    ## Disable all robots crawling.
    location = /robots.txt {
        return 200 "User-agent: *nDisallow: /n";
    }

    ## Protect the documents directory.
    location ^~ /doc/ {
        return 404;
    }

    ## Protect the translation directory.
    location ^~ /po/ {
        return 404;
    }

    ## Protect the attach and data directories. You can comment
    ## out this if these directories are out of the web root in
    ## your setup. That is the default squirrelmail configuration.
    location ^~ /attach/ {
        return 404;
    }

    location ^~ /data/ {
        return 404;
    }

    ## All files/directories that are protected and unaccessible from
    ## the web.
    location ~* ^.*(.(?:git|htaccess|pl|svn|txt))$ {
        return 404;
    }

    ## All READMEs are off limits.
    location ~* README {
        return 404;
    }

    ## All other PHP files are off limits.
    location ~* .php$ {
        return 404;
    }

    ## Fallback to index.php if no file is found.
    try_files $uri $uri/ /index.php;
}
## Return a 1x1 in memory GIF.
    location @empty {
        empty_gif;
    }
}
```

### Roundcube avec sqlite – debian ★

Roundcube est un webmail plus populaire et plus agréable à
l’œil.

![](images/roundcube2.jpg)

![](images/roundcube3.jpg)

Debian dispose de paquets pour roundcube, ce qui rend son installation
nettement plus simple. Cependant, à l’heure où j’écris ces lignes, ils
sont présents dans les backports seulement.

Afin d’ajouter les backports à debian, lancez les commandes suivantes :

```
# echo 'deb http://ftp.debian.org/debian/ jessie-backports main' | \
    tee -a /etc/apt/sources.list.d/backports.list
# apt-get update
```

C’est désormais facile d’installer roundcube avec les métapaquets prévus
à cet effet :

```
# apt-get install roundcube-sqlite3 rondcube-plugins roundcube
```

Lors de cette installation, il vous sera demandé si vous voulez
configurer la base de données pour roundcube. Répondez oui, et
choisissez sqlite3.

Et voilà, il ne vous reste plus qu’à configurer nginx pour roundcube en
vous dirigeant à la partie [Configuration de nginx pour roundcube](#configuration-de-nginx-pour-roundcube).

### Roundcube avec sqlite – archive ★★

On peut aussi installer roundcube à la main, en récupérant l’archive
directement sur le site de roundcube.

Tout d’abord, il faut installer les dépendances

```
    # apt-get install php-pear php5-sqlite php5-fpm php5-apcu \
    php5-mcrypt php5-intl php5-dev php5-gd aspell libmagic-dev sqlite
```

Ensuite, on télécharge roundcube :

```
wget "https://downloads.sourceforge.net/project/roundcubemail/\
roundcubemail/1.1.3/roundcubemail-1.1.3.tar.gz"
```

On extrait roundcube :

```
    tar xvf roundcubemail*.tar.gz
```

On déplace roundcube à l’endroit souhaité :

```
    mv roundcubemail-1.1.3 /var/www/roundcube
```

Enfin, on s’assure que les droits sont corrects :

```
    chown -R www-data:www-data /var/www/roundcube
```

Et voilà, vous pouvez passer à la configuration de nginx pour roundcube
(voir [Configuration de nginx pour roundcube](#configuration-de-nginx-pour-roundcube)). Il faudra juste penser à modifier dans la
configuration de nginx la variable `root` pour mettre l’emplacement des
fichiers de roundcube. Ci-dessus, on avait choisi `/var/www/roundcube`.

Ensuite, ouvrez dans un navigateur https://votredomaine.com/installer
pour terminer l’installation

Après l’installation, supprimez le dossier `installer` dans le dossier
de roundcube avec la commande :

```
    rm -r /var/www/roundcube/installer
```

### Roundcube avec postgresql – debian ★★

La méthode est quasi-identique à l’installation de roundcube avec sqlite
(voir [Roundcube avec sqlite](#roundcube-avec-sqlite-%E2%80%93-debian-%E2%98%85)), à ceci près que l’on va installer le paquet
`roundcube-pgsql` à la place de `roundcube-sqlite3`.

```
# apt-get install postgresql
# apt-get install roundcube-sqlite3 roundcube-plugins roundcube
```

Ensuite, lorsque dbconfig vous demandera la base de données, choisissez
bien sûr postgreSQL.

Il vous sera alors demandé un mot de passe qu’il faudra retenir.

Tout le reste est identique  :).

### Roundcube avec postgresql – archive ★★★

Dans ce cas, toutes les manipulations sont identiques à la section
[Roundcube avec sqlite - archive](#roundcube-avec-sqlite-%E2%80%93-archive-%E2%98%85%E2%98%85) sauf qu’il est inutile d’installer sqlite. Tout ce
qu’on a à voir, c’est comment configurer postgreSQL.

Voici la commande pour installer toutes les dépendances :

```
# apt-get install php5 php-pear php5-fpm php5-apcu\
    php5-mcrypt php5-intl php5-dev php5-gd aspell \
    libmagic-dev php5-pgsql postgresql\
    postgresql-client postgresql-client-common
```

Ensuite, il faut modifier le mot de passe de l’utilisateur postgres qui
sert à configurer postgresql, et créer un nouvel utilisateur pour
roundcube.

Connectez-vous à postgresql avec la commande :

```
# su postgres -c psql
```

puis tapez :

```
postgres=# ALTER USER postgres WITH PASSWORD 'mot_de_passe';
postgres=# CREATE USER "www-data" WITH PASSWORD 'mot_de_passe';
```

Ensuite, on crée la base de données pour roundcube :

```
postgres=# \connect template1
postgres=# CREATE DATABASE "_roundcube" WITH ENCODING 'UTF-8';
postgres=# GRANT ALL PRIVILEGES ON DATABASE "_roundcube" TO "www-data";
postgres=# ALTER DATABASE "_roundcube" OWNER TO "www-data";
```

Puis quittez en tapant ||.

```
/etc/init.d/postgresql restart
```

Et voilà, ça sera tout pour la configuration de postgresql.

Lorsque vous configurerez roundcube à la première connexion, il faudra
alors lui indiquer la base de données `roundcube` avec l’utilisateur
`www-data` selon l’exemple donné ci-dessus.

### Configuration de nginx pour roundcube

Voici un fichier de configuration pour nginx, à enregistrer par exemple
dans `/etc/nginx/conf.d/roundcube.conf`

```
server {
    listen 80;
    server_name webmail.votreserveur.net;
    return 301 https://$server_name$request_uri;  # enforce https
}

server {
    listen 443 ssl http2;
    server_name webmail.votreserveur.net;
    root /var/lib/roundcube;
    index index.php index.html;

    location ~ ^/favicon.ico$ {
        root /var/lib/roundcube/skins/default/images;
        log_not_found off;
        access_log off;
        expires max;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location ~ ^/(README|INSTALL|LICENSE|CHANGELOG|UPGRADING)$ {
        deny all;
    }
    location ~ ^/(bin|SQL)/ {
        deny all;
    }

    location ~ \.php$ {
      try_files $uri = 404;
      fastcgi_pass unix:/var/run/php5-fpm.sock;
      include fastcgi_params;
      fastcgi_intercept_errors on;
      fastcgi_param HTTPS on;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

}
```

Modifiez la variable `server_name` selon l’adresse prévue pour votre
webmail (voir le chapitre sur les [noms de domaine](prerequis.html#un-nom-de-domaine)), puis relancez nginx :

```
# service nginx restart
```

Une fois nginx relancé, vous pourrez alors accéder à roundcube en tapant
dans un navigateur https://webmail.votredomaine.com.

![](images/roundcube-login.png)

Vous verrez alors un écran de connexion. Mettez votre nom d’utilisateur
pour le compte mail et le mot de passe associé. Pour la ligne “Serveur”,
il faut simplement mettre `votredomaine.com`.

Pour ne plus avoir à préciser le serveur, éditez le fichier

`/var/lib/roundcube/config/config.inc.php` puis remplacez

```
$config['default_host'] = '';
```

par

```
$config['default_host'] = 'votredomaine.com';
```

CMS Spip – Exemple détaillé ★★★
---------------------------------------------

[Spip](http://www.spip.net/fr) est un gestionnaire de contenu simplifié,
qui permet notamment de déployer facilement son site web.

![](images/spip.jpg)

### Installation de spip et des dépendances

Prêt pour l’installer? C’est parti!

Puisqu’on utilise debian, on va profiter de sa gestion intelligente des
paquets. On installe tout d’abord le serveur http qui délivrera les
pages web, puis spip.

```
# apt-get install nginx php5-cgi php5-fpm
# apt-get install spip
# rm /etc/nginx/sites-enabled/default
```

En procédant par étape, on évite normalement l’installation d’apache.
Sinon, comme on a déjà nginx, on peut le supprimer (apache) sans
inquiétudes. De la même façon, on supprime le site par défaut qui permet
de tester si nginx fonctionne bien.

N’oubliez pas d’ouvrir le port 80 et/ou 443 si vous utilisez le
chiffrement ssl (voir page ).

### Choix d’une base de données

Selon ce que vous préférez, vous pouvez choisir votre base de données :
MySQL ou SQLite. MySQL est complet et conviendra pour un site
conséquent, mais est plus difficile à mettre en place. SQLite est
extrêmement simple, tout en restant performant. Pour de
l’auto-hébergement, SQLite me semble le choix le plus approprié.

Avec SQLite, installez les paquets sqlite et php5-sqlite :

```
# apt-get install sqlite php5-sqlite
```

Et c’est tout! Vous pouvez passer à la partie suivante.

Pour MySQL, on va installer les paquets nécessaires, puis créer une base
de donnée.

```
# apt-get install mysql-server php5-mysql
```

On vous demandera un mot de passe pour la base de données. Retenez-le
bien. Afin de donner les droits nécessaires à la base de données, il faut
maintenant taper ces commandes :

```
$ mysql -u root -p
    Enter password: * entrez le mot de passe choisi à l'étape précédente*
mysql> CREATE DATABASE spipdb;
mysql> CREATE user 'spip'@'localhost' IDENTIFIED BY 'mot_de_passe_pour_spip';
mysql> GRANT ALL privileges ON spipdb.* TO 'spip'@'localhost';
```

Pour revenir à l’invite de commande normal, appuyez simultanément sur
Ctrl et d.

Quelques explications :

1.  On crée la base de données pour spip, appelée *spipdb*.

2.  On crée un utilisateur *spip* avec son mot de passe.

3.  On accorde à *spip* les droits suffisants sur la base *spipdb*.

### Configuration du serveur http pour spip

Bon, si vous tapez `dpkg -S spip`, vous remarquez que les fichiers sont
installés dans `/usr/share/spip`. Il ne nous reste donc qu’à configurer
nginx de façon adaptée.

On crée donc un fichier nginx-spip.conf dans `/etc/nginx/conf.d/`, pour
y mettre le contenu suivant :

```
server {
    server_name votredomaine.net www.votredomaine.net;
    client_max_body_size 10m;
    root /usr/share/spip;
    index index.php;

    location / {
        try_files $uri $uri/ /spip.php?q=$uri&$args;
    }

    location ~^/(tmp|config|local)/{
        deny all;
        return 403;
    }

    location ~ \.php$ {
      try_files $uri =404;
      fastcgi_pass unix:/var/run/php5-fpm.sock;
      fastcgi_split_path_info ^(.+\.php)(/.+)$;
      fastcgi_index  index.php ;
      fastcgi_buffers 16 16k;
      fastcgi_buffer_size 32k;
      include fastcgi_params;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

}
```

Remplacez `votredomaine.net` par votre nom de domaine, et hop, le tour
est joué! Relancez nginx en tapant : `\# service nginx restart`.

Voyons voir ce qu’il en est à l’adresse http://votredomaine.net. Pour
l’instant, on voit quelque chose comme ça :

![](images/spip-travaux.eps)

On peut donc maintenant passer à la configuration de spip.

Je vous conseille vivement d’ajouter le chiffrement ssl à votre site.
Pour cela, référez vous à la page , puis complétez le fichier
`nginx-spip.conf` ci-dessus ainsi, sans oublier de préciser le nom de
`votrecle.pem`:

```
server {
    listen 80;
    server_name votredomaine.net www.votredomaine.net;
    return 301 https://$server_name$request_uri;  # enforce https
}
server {
    server_name votredomaine.net www.votredomaine.net;
    ssl_certificate /etc/ssl/private/votrecle.pem;
    ssl_certificate_key /etc/ssl/private/votrecle.pem;"
    client_max_body_size 10m;
    root /usr/share/spip;
    index index.php;

    location / {
        try_files $uri $uri/ /spip.php?q=$uri&$args;
    }

    location ~^/(tmp|config|local)/{
        deny all;
        return 403;
    }

    location ~ \.php$ {
      try_files $uri =404;
      fastcgi_pass unix:/var/run/php5-fpm.sock;
      fastcgi_split_path_info ^(.+\.php)(/.+)$;
      fastcgi_index  index.php ;
      fastcgi_buffers 16 16k;
      fastcgi_buffer_size 32k;
      include fastcgi_params;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

}
```

### Configuration de spip

Comme il est indiqué dans le fichier
`/usr/share/doc/spip/README.Debian`, que bien sûr, vous avez lu
(n’est-ce pas toi là-bas, au fond de la classe?), il faut ajouter le
site spip en tapant cette commande :

```
# spip_add_site votredomaine.net
```

Vous obtenez alors une réponse qui ressemble à :

```
Creating site votredomaine.net...
Directories and files created, you may add hosts for this site in:
   /etc/spip/sites/votredomaine.net.php
```

Dirigez-vous maintenant à l’adresse http://votredomaine.net/ecrire pour
terminer l’installation.

Si vous choisissez une base de données SQLite, c’est extrêmement simple,
les choix par défaut sont compréhensibles et suffisants.

Si vous choisissez une base MySQL, quelques explications s’imposent :

![](images/spipinstall1.png)

-   Adresse de la base de données : localhost
-   Login de connexion : c’est l’utilisateur créé tout à l’heure. Dans
    l’exemple, il s’appelait *spip*.
-   Le mot de passe de connexion : il s’agit du mot de passe pour
    l’utilisateur *spip*. Dans l’exemple, c’était
    `mot_de_passe_pour_spip`.

Ensuite, choisissez de sélectionner la base de données déjà créée (on
l’avait appelée spipdb).

![](images/spipinstall2.png)

La suite est très classique :

![](images/spipinstall3.png)

![](images/spipinstall4.png)

Ne vous inquiétez pas de l’avertissement au sujet du fichier htaccess.
On s’en est en fait déjà occupé dans la configuration de nginx.

Il ne reste plus qu’à vous connecter, puis d’administrer votre site.

![](images/spipinstall5.png)

![](images/spipinstall6.png)

Un Wiki avec Dokuwiki
---------------------

[Dokuwiki](https://www.dokuwiki.org/dokuwiki) est un moteur de wiki
libre, facile à utiliser qui ne nécessite pas beaucoup de ressources et
aucune base de données.

Il dispose en plus de nombreux greffons qui permettent de lui ajouter
tout un tas de fonctionnalités.

![](images/dokuwiki.png)

### Méthode debian ★

Nous allons ici décrire l’installation de
[dokuwiki](https://www.dokuwiki.org/dokuwiki) à l’aide des paquets
debian.

Rien de bien compliqué, il faut juste installer le paquet `dokuwiki` :

```
# apt-get install dokuwiki
```

Quelques questions vont vous être posées, comme par exemple le mot de
passe de l’administrateur du wiki.

Vous pouvez maintenant passer à la [configuration de nginx pour dokuwiki](#configuration-de-nginx-pour-dokuwiki).

Lors de votre première connexion, le nom d’utilisateur est “admin” et le
mot de passe celui définit précédemment.

### Méthode avec l’archive ★★

Cette méthode un peu plus contraignante présente l’avantage de pouvoir
installer plusieurs wiki sur son serveur.

Tout d’abord, on installe le nécessaire au bon fonctionnement de
dokuwiki :

```
# apt-get install php5-apcu php5-gd \
imagemagick php-geshi php-seclib
```

Ensuite, on récupère l’archive de dokuwiki :

```
wget "http://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz"
```

On décompresse l’archive

```
tar xvf dokuwiki-stable.tgz
```

Maintenant, on déplace le dossier de dokuwiki à un emplacement qui nous
convient mieux, par exemple `/var/www/dokuwiki`

```
mv dokuwiki-* /var/www/dokuwiki
```

Enfin, on règle les droits sur le dossier de dokuwiki

```
chown -R www-data:www-data /var/www/dokuwiki
```

Et voilà, vous pouvez maintenant [configurer nginx pour dokuwiki](#configuration-de-nginx-pour-dokuwiki).

### Configuration de nginx pour dokuwiki

Voici la configuration de nginx pour dokuwiki, à enregistrer par exemple
dans `/etc/nginx/conf.d/dokuwiki.conf`.

Si vous utilisez le paquet debian, laissez la variable `root` à
`/usr/share/dokuwiki`. Sinon, modifiez ce chemin vers l’emplacement où
vous avez décompressé l’archive de dokuwiki.

De même, la variable `server_name` est à modifier selon le domaine
choisi pour accéder à votre wiki.

Enfin, adaptez les variables pour le certificat ssl selon la
configuration effectuée a la section [ssl](#obtenir-un-certificat-ssl).

```
server {
    listen 80;
    server_name monwiki.com;
    return 301 https://$server_name$request_uri;  # enforce https
}
server {
    listen      443 ssl;
    ssl_certificate /etc/ssl/private/mondomaine.pem;
    ssl_certificate_key /etc/ssl/private/mondomaine.pem;"
    server_name monwiki.com;
    root /usr/share/dokuwiki;
    client_max_body_size 1500M;
    index index.html index.php doku.php;

    location ~ \.php$ {
        try_files $uri = 404;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        include fastcgi_params;
        fastcgi_intercept_errors on;
        fastcgi_param HTTPS on;
        fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
    }

    location / { try_files $uri $uri/ @dokuwiki; }

    location @dokuwiki {
            rewrite ^/_media/(.*) /lib/exe/fetch.php?media=$1 last;
            rewrite ^/_detail/(.*) /lib/exe/detail.php?media=$1 last;
            rewrite ^/_export/([^/]+)/(.*) /doku.php?do=export_$1&id=$2 last;
            rewrite ^/(.*) /doku.php?id=$1&$args last;
    }


    location ~ /(data|conf|bin|inc)/ {
        deny all;
    }

# serve static files
    location ~ ^/dokuwiki/lib/^((?!php).)*$  {
    root         /usr/share/dokuwiki/lib;   #adapt if needed
    expires 30d;
    }
}
```

Recharger nginx pour accéder à votre site :

```
# service nginx restart
```

Un autre forum : FluxBB ★★★
-----------------------------------------

<span>m<span>0.8</span> m<span>0.2</span></span>
[FluxBB](https://fluxbb.org/) est un moteur de forum complet, qui
nécessite une base de données. On va décrire ici l’installation avec
PostgreSQL, bien qu’il soit tout à fait possible d’utiliser MySQL en
suivant les indications du [BDD].

![](images/fluxbb.png)

Installons tout d’abord les dépendances pour fluxbb :

```
# apt-get install php5-fpm php5-apcu php-pear php-db\
php5-pgsql postgresql postgresql-client postgresql-client-common
```

On configure ensuite postgresql. Il faut modifier le mot de passe de
l’utilisateur postgres qui sert à configurer postgresql, et créer un
nouvel utilisateur pour fluxbb.

Connectez-vous à postgresql avec la commande :

```
# su postgres -c psql
```

puis tapez :

```
postgres=# ALTER USER postgres WITH PASSWORD 'mot_de_passe';
postgres=# CREATE USER "fluxbbuser" WITH PASSWORD 'mot_de_passe';
```

Ensuite, on crée la base de données pour fluxbb :

```
postgres=# \connect template1
postgres=# CREATE DATABASE "_fluxbbdb" WITH ENCODING 'UTF-8';
postgres=# GRANT ALL PRIVILEGES ON DATABASE "_fluxbbdb" TO "fluxbbuser";
postgres=# ALTER DATABASE "_fluxbbdb" OWNER TO "fluxbbuser";
```

Puis quittez en tapant ||.

Relancez postgresql :

```
/etc/init.d/postgresql restart
```

Et voilà, ça sera tout pour la configuration de postgresql.

Lorsque vous configurerez fluxbb, il faudra alors lui indiquer la base
de données `fluxbbdb` avec l’utilisateur `fluxbbuser` selon l’exemple
donné ci-dessus.

Vous pouvez récupérer l’archive de fluxbb en consultant cette page
<https://fluxbb.org/downloads/>. À l’heure où ces lignes sont écrites,
c’est la version 1.5.8 qui est disponible.

```
wget https://fluxbb.org/download/releases/1.5.8/fluxbb-1.5.8.tar.gz
```

Ensuite, on décompresse l’archive, et on la déplace dans
`/var/www/fluxbb`. On n’oubliera pas de modifier les droits sur ce
dossier :

```
tar xvf fluxbb-1.5.8.tar.gz
mv fluxbb-1.5.8 /var/www/fluxbb
chown -R www-data:www-data /var/www/fluxbb
```

On peut finalement ajouter ce fichier de configuration à nginx, à
enregistrer par exemple dans `/etc/nginx/conf.d/fluxbb.conf`. N’oubliez
pas de modifier les variables `root`, `server_name` et `ssl_*`.

```
server {
    listen 80;
    server_name forumquidechire.com;
    return 301 https://$server_name$request_uri;  # enforce https
}
server {
    listen 443 ssl http2;
    ssl_certificate /etc/ssl/private/mondomaine.pem;
    ssl_certificate_key /etc/ssl/private/mondomaine.pem;"
    index index.php;
    root /var/www/fluxbb;

    location ~ \.php$ {
        try_files $uri = 404;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        include fastcgi_params;
        fastcgi_intercept_errors on;
        fastcgi_param HTTPS on;
        fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
    }
}
```

Redémarrez nginx avec la commande `\# service nginx restart`, puis
digirez-vous à l’adresse https://votreforum.com/install.php pour
terminer l’installation.

Un pastebin chiffré : ZeroBin ★
-------------------------------------

ZeroBin permet de partager du texte et d’en discuter tout en chiffrant
le contenu afin de garder l’ensemble le plus discret possible.

Il est nécessaire d’avoir php et nginx d’installés pour l’utiliser :

```
# apt-get install nginx php5 php5-fpm php5-gd unzip
```

Ensuite, on va récupérer l’archive de zerobin et la décompresser :

```
wget "https://github.com/sebsauvage/ZeroBin/archive/master.zip"
unzip master.zip
```

On peut maintenant déplacer les fichiers de zerobin à un emplacement
prévu pour votre serveur, par exemple `/var/www/zerobin`. On corrigera
les droits par la même occasion.

```
mv ZeroBin-master /var/www/zerobin
chown -R www-data:www-data /var/www/zerobin
```

Il ne reste plus qu’à ajouter la configuration de nginx dans, par
exemple, `/etc/nginx/conf.d/zerobin.conf`.

```
    server {
    listen 80 ;
    server_name pastebin.monserveur.net;
    root /var/www/zerobin;
    index index.php;
    location ~ \.php$ {
        try_files $uri = 404;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        include fastcgi_params;
        fastcgi_intercept_errors on;
        fastcgi_param HTTPS on;
        fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
    }
}
```

Comme d’habitude, pensez à adapter à votre cas les variables `root` et
`server_name`.

Relancez nginx puis profitez de votre nouveau pastebin.

```
# service nginx restart
```

Partage de liens avec Shaarli ★
-------------------------------------

[Shaarli](http://sebsauvage.net/wiki/doku.php?id=php:shaarli) est une
application qui permet de partager et mettre de côté des liens que l’on
trouve intéressants. Certains s’en servent même de blog ou de bloc-note.

![](images/shaarli.png)

Vous allez voir que l’installation ressemble beaucoup à celle de [zerobin](#un-pastebin-chiffr%C3%A9--zerobin-%E2%98%85), puisque c’est le même développeur qui l’a créé.

Il est aussi nécessaire d’avoir php et nginx d’installés pour l’utiliser :

```
# apt-get install nginx php5 php5-fpm php5-gd unzip
```

Ensuite, on va récupérer l’archive de shaarli et la décompresser :

```
wget "https://github.com/shaarli/Shaarli/archive/master.zip"
unzip master.zip
```

On peut maintenant déplacer les fichiers de shaarli à un emplacement
prévu pour votre serveur, par exemple `/var/www/shaarli`. On corrigera
les droits par la même occasion.

```
mv Shaarli-master /var/www/shaarli
chown -R www-data:www-data /var/www/shaarli
```

Il ne reste plus qu’à ajouter la configuration de nginx dans, par
exemple, `/etc/nginx/conf.d/shaarli.conf`.

```
server {
    listen 80;
    server_name links.monserveur.net;
    return 301 https://$server_name$request_uri;  # enforce https
}

server {
    listen      443 ssl;
    ssl_certificate /etc/ssl/private/mondomaine.pem;
    ssl_certificate_key /etc/ssl/private/mondomaine.pem;"
    server_name links.monserveur.net;
    root /var/www/shaarli;
    index index.php;
    location ^~ /cache {
        deny all;
        return 403;
    }
    location ^~ /data {
        deny all;
        return 403;
    }
    location ~ \.php$ {
      try_files $uri = 404;
      fastcgi_pass unix:/var/run/php5-fpm.sock;
      include fastcgi_params;
      fastcgi_intercept_errors on;
      fastcgi_param HTTPS on;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
```

Comme d’habitude, pensez à adapter à votre cas les variables `root`,
`server_name` et celles concernant le certificat ssl `ssl_*`.

Relancez nginx puis profitez de votre nouveau shaarli.

```
# service nginx restart
```

Sauvegardez vos articles avec Wallabag ★★
---------------------------------------------------

[Wallabag](https://www.wallabag.org) est une application de
“Read-it-later”. Cela vous permet de sauvegarder des pages web afin de
les lire plus tard, ou tout simplement pour les garder bien au chaud.

Pour le faire fonctionner, il faudra installer les paquets suivants :

```
php5 php5-sqlite openssl ssl-cert php5-fpm \
php5-apcu php5-curl unzip php5-mcrypt php5-tidy php5-cli curl
```

Supposons que vous voulez enregistrer wallabag dans le dossier
`/var/www/wallabag`. Il faut donc créer ce dossier :

```
# mkdir -p /var/www/wallabag
```

On peut maintenant le télécharger et décompresser wallabag :

```
wget "http://wllbg.org/latest" -O wallabag.zip
unzip wallabag.zip
```

Déplaçons maintenant les fichiers de wallabag dans /var/www/wallabag :

```
mv wallabag-/* /var/www/wallabag
```

Il ne reste plus qu’à installer une dépendance à wallabag. Ces commandes
vont la télécharger et l’installer après s’être déplacé dans le dossier
contenant wallabag:

```
cd /var/www/wallabag
curl -s http://getcomposer.org/installer | php
php composer.phar install
```

Enfin, corrigez les droits pour les fichiers de wallabag ainsi :

```
chown -R www-data:www-data /var/www/wallabag
```

La configuration de nginx pour wallabag n’a rien de particulier, c’est
un simple site utilisant php. Créez alors par exemple le fichier
/etc/nginx/conf.d/wallabag.conf avec dedans :

```
server {
  listen 80;
  server_name wallabag.mondomaine.net;
  return 301 https://$server_name$request_uri;  # enforce https
}

server {
  listen      443 ssl;
  ssl_certificate /etc/ssl/private/mondomaine.pem;
  ssl_certificate_key /etc/ssl/private/mondomaine.pem;"
  server_name wallabag.mondomaine.net;
  root /var/www/wallabag;
  index index.php;
      location ~ \.php$ {
      try_files $uri = 404;
      fastcgi_pass unix:/var/run/php5-fpm.sock;
      include fastcgi_params;
      fastcgi_intercept_errors on;
      fastcgi_param HTTPS on;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

}
```

Les variables commençant par `ssl_` seront à adapter à votre certificat
(voir la section [ssl](#obtenir-un-certificat-ssl)).

Il ne reste plus qu’à redémarrer nginx pour pouvoir profiter de wallabag
à l’adresse https://wallabag.mondomaine.net.

```
service nginx restart
```

Votre nuage avec Owncloud
-------------------------

[Owncloud](https://owncloud.org/) vous permet de stocker et partager vos
documents en ligne, et éventuellement de les synchroniser. Vous pourrez
aussi profitez de services Carddav et Caldav pour vos rendez-vous.

![](images/owncloud.jpg)

Il faut bien sûr que nginx et php soient déjà installés avant de passer
à la suite (voir la section [nginx](installation.html#installation-de-nginx)).

On expliquera ici l’installation d’owncloud avec la base de donnée
SQLite. Si vous souhaitez en utiliser une autre comme mysql, c’est tout
à fait possible. Vous devrez cependant la créer au préalable à la main,
comme indiqué au chapitre [Les bases de données](bdd.html).

### Installation des dépendances à owncloud

Afin de profiter d’un maximum de fonctionnalités avec owncloud, vous
devriez installer les paquets suivants :

```
php5-gd php-xml-parser php5-intl curl libcurl3 php5-curl php5-dev
php5-fpm php5-cli php5-sqlite php5-common php5-cgi sqlite
php5-apcu bzip2 php5-mcrypt php5-json bzip2 unzip php-geshi
fonts-font-awesome fonts-liberation fonts-linuxlibertine
fonts-lohit-deva fonts-sil-gentium-basic fonts-wqy-microhei
libjs-chosen libjs-dojo-dojox libjs-jcrop libjs-jquery-minicolors
libjs-jquery-mousewheel libjs-jquery-timepicker libjs-mediaelement
libjs-pdf libjs-underscore libphp-phpmailer php-assetic
php-doctrine-dbal php-getid3 php-opencloud php-patchwork-utf8
php-pear php-pimple php-sabre-dav php-seclib
php-symfony-classloader php-symfony-console php-symfony-routing
php-crypt-blowfish
```

Cette liste, déjà relativement grande, peut être complétée par les
paquets suivants pour profiter de quelques aperçus pour les images et
autres documents :

```
php5-imagick imagemagick libreoffice libav-tools vorbis-tools
```

### Installation avec les paquets debian ★

Pour installer owncloud, on va se servir des paquets debian :

```
# apt-get install owncloud
```

Vous pouvez aussi installer en plus le paquet `owncloud-apps` qui
contient des extensions.

Une fois ceci fait, il ne reste plus qu’à passer à la configuration de
nginx pour owncloud à la section [Configuration de nginx pour owncloud](configuration-de-nginx-pour-owncloud).

### Installation avec l’archive ★★

Cette méthode permet d’avoir la version la plus à jour d’owncloud.

On va tout d’abord télécharger owncloud. Vous pouvez retrouver l’adresse
de téléchargement à cette page : <https://owncloud.org/install/>.

```
wget "https://download.owncloud.org/community/owncloud-8.1.3.tar.bz2"
```

Ensuite, les commandes suivantes vont permettre de

-   décompresser owncloud,
-   le déplacer dans `/var/www/owncloud`,
-   créer les répertoires nécessaires au bon fonctionnement de owncloud
-   corriger les droits

```
tar xvjf owncloud-*.tar.bz2
mv owncloud /var/www/
mkdir -p "/var/www/owncloud/"{apps,data,config}
chown -R www-data:www-data "/var/www/owncloud/"{apps,data,config}
```

Et voilà!

Vous pouvez maintenant passer à la configuration de nginx pour owncloud
afin de terminer l’installation.

### Configuration de nginx pour owncloud

Voici le fichier de configuration de nginx pour un site owncloud.
N’hésitez pas à vérifier d’éventuelles mises à jour sur la documentation
officielle située ici :
<https://doc.owncloud.org/server/8.2/admin_manual/installation/nginx_configuration.html>

```
# redirect http to https.
server {
  listen 80;
  server_name cloud.mondomaine.com;
  return 301 https://$server_name$request_uri;  # enforce https
}

# owncloud (ssl/tls)
server {
listen 443 ssl http2;
ssl_certificate /etc/ssl/private/mondomaine.pem;
ssl_certificate_key /etc/ssl/private/mondomaine.pem;"
server_name cloud.mondomaine.com;
root /usr/share/owncloud;
index index.php;
client_max_body_size 1500M; # set maximum upload size
fastcgi_buffers 64 4K;

# Add headers to serve security related headers
add_header Strict-Transport-Security "max-age=15768000; \
    includeSubDomains; preload;";
add_header X-Content-Type-Options nosniff;
add_header X-Frame-Options "SAMEORIGIN";
add_header X-XSS-Protection "1; mode=block";
add_header X-Robots-Tag none;

# Disable gzip to avoid the removal of the ETag header
gzip off;

rewrite ^/caldav(.*)$ /remote.php/caldav$1 redirect;
rewrite ^/carddav(.*)$ /remote.php/carddav$1 redirect;
rewrite ^/webdav(.*)$ /remote.php/webdav$1 redirect;

error_page 403 /core/templates/403.php;
error_page 404 /core/templates/404.php;

location = /robots.txt {
    allow all;
    log_not_found off;
    access_log off;
}
location / {
  # The following 2 rules are only needed with webfinger
  rewrite ^/.well-known/host-meta /public.php?service=host-meta last;
  rewrite ^/.well-known/host-meta.json /public.php?service=host-meta-json last;

  rewrite ^/.well-known/carddav /remote.php/carddav/ redirect;
  rewrite ^/.well-known/caldav /remote.php/caldav/ redirect;

  rewrite ^(/core/doc/[^\/]+/)$ $1/index.html;

  try_files $uri $uri/ index.php;
}

location ~ ^/(?:\.htaccess|data|config|db_structure\.xml|README){
deny all;
}

location ~ \.php(?:$|/) {
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_param PATH_INFO $fastcgi_path_info;
    fastcgi_param HTTPS on;
    fastcgi_pass unix:/var/run/php5-fpm.sock;
}

# Adding the cache control header for js and css files
# Make sure it is BELOW the location ~ \.php(?:$|/) { block
location ~* \.(?:css|js)$ {
    add_header Cache-Control "public, max-age=7200";
    # Add headers to serve security related headers
    add_header Strict-Transport-Security "max-age=15768000;\
        includeSubDomains; preload;";
    add_header X-Content-Type-Options nosniff;
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Robots-Tag none;
    # Optional: Don't log access to assets
    access_log off;
}

# Optional: set long EXPIRES header on static assets
location ~* ^.+\.(jpg|jpeg|gif|bmp|ico|png|css|js|swf)$ {
    expires 30d;
    # Optional: Don't log access to assets
    access_log off;
}
}
```

Remplacez les variables commençant par `ssl_` pour indiquer où se
trouve votre certificat et la variable `server_name` pour y mettre
votre nom de domaine.

Si vous avez choisi une installation à partir de l’archive et non à
partir des paquets debian, il faut aussi modifier la variable `root`
pour y mettre le chemin vers les fichiers de owncloud.

Référez vous à la section [Limites en upload pour php](#limites-en-upload-pour-php) pour augmenter la taille maximale des
fichiers pouvant être “uploadés” sur votre serveur.

Il ne vous reste plus qu’à relancer nginx pour profiter de votre cloud.

```
# service nginx restart
```

Dépôt de fichier avec Jirafeau ★
--------------------------------------

Jirafeau est un service qui permet de déposer un fichier sur votre
serveur, puis de pouvoir le partager en donnant un lien unique. Vous
pouvez l’utiliser en alternative à dl.free.fr, mega ou framadrop.

Pour l’installer, on commence par installer les paquets nécessaire à
jirafeau :

```
nginx php5 openssl ssl-cert php5-fpm php5-apcu
```

Ensuite, on récupère l’archive de jirafeau avec wget :

```
wget "https://gitlab.com/mojo42/Jirafeau/repository/archive.zip"
```

On peut maintenant décompresser l’archive :

```
unzip archive.zip
```

Un dossier commençant par “Jirafeau-master-” est créé.

Supposons que vous souhaitez mettre les fichiers de jirafeau dans
`/var/www/jirafeau`, on va alors créer ce dossier puis déplacer les
fichiers de Jirafeau dedans :

```
mkdir -p /var/www/jirafeau
mv Jirafeau-master-*/* /var/www/jirafeau
```

Enfin, modifiez les droits pour plus de sécurité :

```
chown -R www-data:www-data /var/www/jirafeau
```

Il ne reste plus qu’à configurer nginx. Rien de particulier n’est à
faire, c’est un site en php comme décrit à la section concernant [php](installation.html#installation-de-nginx). Modifiez toutefois la
variable `root` pour qu’elle corresponde à `/var/www/jirafeau` ainsi que
les variables commençant par `ssl` pour indiquer votre certificat (voir
la section [ssl](#obtenir-un-certificat-ssl).

Au final, le fichier de configuration de nginx ressemblera à ça :

```
server {
  listen 80;
  server_name drop.domaine.com;
  return 301 https://$server_name$request_uri;  # enforce https
}

server {
  add_header  Cache-Control public;
  expires     max;
  listen      443 ssl;
  ssl_certificate /etc/ssl/private/mondomaine.pem;
  ssl_certificate_key /etc/ssl/private/mondomaine.pem;"
  server_name drop.domaine.com;
  root /var/www/jirafeau ;
  index index.php;
  client_max_body_size 1500M;
  location ~ \.php$ {
      try_files $uri = 404;
      fastcgi_pass unix:/var/run/php5-fpm.sock;
      include fastcgi_params;
      fastcgi_intercept_errors on;
      fastcgi_param HTTPS on;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
```

Référez vous au [phplimit] pour augmenter la taille maximale des
fichiers pouvant être “uploadés” sur votre serveur.

Vous pouvez maintenant relancer nginx puis aller sur votre nouveau site
pour terminer l’installation.

Statistiques sur votre site
---------------------------

### Avec Webalizer ★★

Webalizer est un outil pour obtenir des statistiques sur votre site. Il
suffira de l’installer avec apt-get, puis d’ajouter ces lignes dans le
fichier de configuration de nginx, afin d’accéder aux statistiques à la
page http://nom-du-serveur/Divers/stats/index.html.

```
# Pour Webalizer
location /Divers/stats {
    alias /var/www/webalizer;
}
```

Cela suppose que les fichiers de webalizer ont été générés dans
`/var/www/webalizer`.

Vous pouvez surveiller plusieurs domaines. Pour cela, copier le fichier
de configuration de webalizer par défaut, en utilisant un fichier par
nom de domaine à surveiller :

```
cp /etc/webalizer/webalizer.conf.sample /etc/webalizer/domaine1.conf
cp /etc/webalizer/webalizer.conf.sample /etc/webalizer/domaine2.conf
cp /etc/webalizer/webalizer.conf.sample /etc/webalizer/domaine3.conf
```

Créez ensuite les dossiers pour accueillir les pages générées par
webalizer, et donnez l’accès à ces dossiers à nginx :

```
mkdir -p /var/www/webalizer-domaine1
mkdir -p /var/www/webalizer-domaine2
mkdir -p /var/www/webalizer-domaine3
chown -R www-data:www-data /var/www/webalizer-domaine*
```

Adaptez ces nouveaux fichiers de configuration, en particulier changez
les variables suivantes :

-   `LogFile` : Le chemin vers le fichier de log pour ce domaine :
    `/var/log/nginx/domaine1-access.log`
-   `OutputDir` : mettez le dossier correspondant :
    `/var/www/webalizer-domaine1` par exemple. Il faudra alors modifier
    la configuration de nginx pour qu’il aille chercher les fichiers de
    webalizer dans le nouveau dossier.
-   `HostName` : le nom de domaine concerné.

Finalement, ajoutez ces lignes dans le fichier `/etc/logrotate.d/nginx`
juste avant “prerotate”

```
webalizer -c /etc/webalizer/domaine1.conf >/dev/null 2>&1
webalizer -c /etc/webalizer/domaine2.conf >/dev/null 2>&1
webalizer -c /etc/webalizer/domaine3.conf >/dev/null 2>&1
```

### Avec Piwik ★★★

Piwik va vous permettre de mettre un mouchard sur les pages de votre
site afin d’obtenir des statistiques sur les visites reçues. Il
nécessite l’utilisation de mysql (voir le chapitre [Les bases de données](bdd.html)

![](images/piwik.png)

Piwik aura besoin des paquets suivants :

```
nginx php5-curl php5-gd php5-cli
php5-geoip php5-fpm php5-mysql mysql-server-5.5
```

Créez une nouvelle base de données pour piwik comme indiqué au
[gestmysql](~TODO).

Récupérez l’archive de piwik, puis déplacez-là où vous souhaitez, par
exemple dans `/var/www/piwik`. On modifie les droits comme il faut pour
finir :

```
wget 'http://builds.piwik.org/piwik.zip'
unzip piwik.zip
mv piwik /var/www/piwik
chown -R www-data:www-data /var/www/piwik
```

Visitez ensuite cette page pour obtenir une configuration de nginx
complète et sécurisée : <https://github.com/perusio/piwik-nginx/>.

### Graphiques sur la charge du serveur avec munin ★★★

Munin permet de surveiller votre serveur à l’aide de graphiques montrant
la charge du processeur, la quantité de mémoire utilisée…

Tout d’abord, on installe les paquets pour munin :

```
apt install munin munin-node munin-plugins-extra
```

La configuration de nginx quant à elle est :

```
server {
    listen 80;
    server_name munin.domaine.tld;
    access_log /var/log/nginx/acces_munin.log;
    error_log /var/log/nginx/error_munin.log;

    server_name_in_redirect off;
    root /var/cache/munin/www/;

    location /munin/static/ {
        alias /etc/munin/static/;
        expires modified +1w;
    }
}
```

Les plugins pour munin sont disponibles dans le répertoire :
/usr/share/munin/plugins/.

Pour lister les plugins disponible :

```
munin-node-configure
```

Pour activer un plugin, il faut créer un lien symbolique vers le
répertoire munin :

```
ln -s /usr/share/munin/plugins/NOM-PLUGINS /etc/munin/plugins/NOM-PLUGINS
```

Relancer munin-node pour la prise en compte :

```
systemctl restart munin-node
```

Par exemple, pour nginx, on peut activer son support ainsi :

```
ln -s /usr/share/munin/plugins/nginx_request /etc/munin/plugins/nginx_request
ln -s /usr/share/munin/plugins/nginx_status /etc/munin/plugins/nginx_status
```

Bon, là, il faut rajouter ces lignes dans la configuration de nginx pour
le site à surveiller :

```
location /nginx_status {
    stub_status on;    # activate stub_status module
    access_log off;
    allow 127.0.0.1;   # restrict access to local only
    deny all;
}
```

Cette partie est largement inspirée de [ce
site](https://www.scalescale.com/tips/nginx/monitor-nginx-munin/).

Limites en upload pour php
--------------------------

Par souci de sécurité, debian limite la taille des fichiers qui peuvent
être envoyés sur votre serveur. Vous pouvez bien entendu augmenter cette
limite.

Pour cela, éditez le fichier `/etc/php5/fpm/php.ini` pour modifier la
valeur des variables suivantes :

```
upload_max_filesize = 1500M
post_max_size = 1500M
```

Une fois vos modifications effectuées, relancez php ainsi :

```
service php5-fpm restart
```

Notez qu’il faudra aussi ajouter une ligne dans la configuration de
nginx :

```
client_max_body_size 1500M;
```

[^1]: <https://letsencrypt.org/getting-started/>

